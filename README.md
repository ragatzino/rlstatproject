# RLStatproject

Ce projet a pour but de mettre en application les différentes notions vu en cours de conception logiciel, technologies mobiles et datavisualisation de l'ENSAI.
test1

## Objetifs
Fan du jeu Rocket League, cette application a pour but de résumer les matchs importants de la scène eSport de ce jeu.
L'application reprend les idées du site octane.gg et à pour but de les améliorer à termes en exposant des graphiques et un modèle de prédiction.

Les données devront être basé sur une API fait maison qui sera remplie 'à la main' dans un premier temps. Une petite application python nous permettra d'insérer les nouvelles données des matchs le plus aisément possible. Une version finale serait d'insérer une image qui résume les scores du match et remplie l'API automatiquement. L'API sera créer à l'aide du module fastAPI. Les premières données provient du web scrapping du site octagne.gg.

Dans un second temps, nous créerons un site internet, adaptatif sur ordinateur et mobile, à l'aide de REACT. On pouura créer des pages sans avoir à les créer au préalablement grâce à notre API. Les pages seront auto-générées.

Enfin, une analyse statistique des matchs sera produite afin de déterminer un modèle de prédiction de score entre deux équipes.

# Utilisation
Dans un premier temps, veuillez installer les dépendances du projet via la commande suivante : pip install -r requirements.txt
Expliquons le fonctionnement de la base de données. Les données seront enregistrées dans une base SQLite par une API gérée grâce au module fastapi. Une mise en place d'ORMs est nécéssaire. Tout est contenu dans le package sql_app.
Pour lancer la base de données et faire des requête, faire la commande suivante à la racine du projet: uvicorn sql_app.main:app --reload
Ensuite, insérer dans votre navigateur l'adresse suivante, http://127.0.0.1:8000/docs
Uvicorn va vous créer un seerveur local où vous pourrez faire tout les tests possibles. Vous y trouverez toutes les commandes réalisables.
