# On importe la fonction 'get' (téléchargement) de 'requests'
# Et la classe 'Selector' (Parsing) de 'scrapy'
from requests import get
from scrapy import Selector
import numpy as np
import pandas as pd

df1 = pd.read_csv(r"myfile.csv", sep=";")

# Lien de la page à scraper
url = "https://liquipedia.net/rocketleague/Rocket_League_Championship_Series/Season_X/Winter/North_America"
response = get(url)
source = None # Le code source de la page
list = []

if response.status_code == 200 :
    # Si la requete s'est bien passee
    source = response.text
    print(source)
if source :
    # Si le code source existe
    selector = Selector(text=source)
    titles = selector.css("div.bracket-game")
    print(titles)
    for title in titles:
        nameteams = title.css("span.team-template-text::text").getall()
        scores = title.css("div.bracket-score::text").getall()
        match = nameteams + scores
        print(match)
        if len(match) == 4:
            list.append(match)
        if len(match) == 6:
            l1 = [match[0], match[1], match[2], match[4]]
            l2 = [match[0], match[1], match[3], match[5]]
            list.append(l1)
            list.append(l2)
    ar = np.array(list)
    df2 = pd.DataFrame(list, columns=['Team1', 'Team2', 'ScoreTeam1', 'ScoreTeam2'])

df3 = pd.concat([df1, df2], ignore_index=True)

file = df3.to_csv('myfile.csv', header = True, index = False, sep = ';')