from scrapy import Selector
import pandas as pd
import glob

Lienhtml = glob.glob("matches/*.html")

df0 = pd.DataFrame(columns=['idrencontre', 'Nom de la competition', 'Type du match', 'Date', 'Team1', 'Team2', 'ScoreTeam1', 'ScoreTeam2', 'player1', 'player2', 'player3', 'player4', 'player5', 'player6',
                           'SCPG1', 'GPG1', 'APG1', 'SAPG1', 'SHPG1', 'SCPG2', 'GPG2', 'APG2', 'SAPG2', 'SHPG2', 'SCPG3', 'GPG3', 'APG3', 'SAPG3', 'SHPG3', 'SCPGT1', 'GPGT1', 'APGT1', 'SAPGT1', 'SHPGT1', 'SCPG4', 'GPG4', 'APG4', 'SAPG4', 'SHPG4', 'SCPG5', 'GPG5', 'APG5', 'SAPG5', 'SHPG5', 'SCPG6', 'GPG6', 'APG6', 'SAPG6', 'SHPG6', 'SCPGT2', 'GPGT2', 'APGT2', 'SAPGT2', 'SHPGT2',
                           'rating1', 'rating2', 'rating3', 'rating4', 'rating5', 'rating6'])

df = pd.read_csv(r"myfile3.csv", sep=";")

for lien in Lienhtml:
    f = open(lien, 'r')
    message = f.read()

    selector = Selector(text=message)

    matchinfo1 = [selector.css("div.match-info-section-event::text").get()]
    matchinfo2 = selector.css("div.match-info-section-text::text").getall()[:2]

    statmatch = selector.css("div.scoreboard").get()

    getnameteam = selector.css("div.match-team-header").getall()[:2]

    nameteam1 = [Selector(text=getnameteam[0]).css("div::text").get()[1:-2]]
    nameteam2 = [Selector(text=getnameteam[1]).css("div::text").get()[1:-2]]

    score1 = [Selector(text=getnameteam[0]).css("span::text").get()]
    score2 = [Selector(text=getnameteam[1]).css("span::text").get()]

    getinfo = Selector(text = statmatch)

    playernamesbefore = getinfo.css("a::text").getall()
    playersname = playernamesbefore[:3] + playernamesbefore[4:7]

    playerscoresbefore = getinfo.css("td::text").getall()
    playerscores = []
    for element in playerscoresbefore:
        newelement = element.strip()
        playerscores.append(newelement)

    playersrating = getinfo.css("b::text").getall()

    idrencontre = [len(df) + 1]
    L = idrencontre + matchinfo1 + matchinfo2 + nameteam1 + nameteam2 + score1 + score2 + playersname + playerscores + playersrating
    print(L)
    df.loc[idrencontre[0]-1] = L

print(df)
file = df.to_csv('myfile3.csv', header = True, index = False, sep = ';')