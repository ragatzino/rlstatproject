import pandas as pd
import numpy as np

df = pd.read_csv(r"myfile.csv", sep=";")
L1 = df['Team1'].unique()
L2 = df['Team2'].unique()
L = np.concatenate((L1, L2), axis=0)

def obtainallequipes():
    equipeall = []
    for i in L :
        if i not in equipeall:
            equipeall.append(i)
    print("nombre de participants :", )
    print("liste de tous les participants :", equipeall)

def statequipe(nomequipe):
    table1 = df[df['Team1'] == nomequipe]
    table2 = df[df['Team2'] == nomequipe]
    table = df[((df['Team1'] == nomequipe) | (df['Team2'] == nomequipe))]

    s, v = len(table), 0

    for i in table1.index:
        if table1.loc[i]['ScoreTeam1'] > table1.loc[i]['ScoreTeam2']:
            v += 1

    for i in table2.index:
        if table2.loc[i]['ScoreTeam2'] > table2.loc[i]['ScoreTeam1']:
            v += 1

    print(nomequipe)
    print("Nombre de matchs joués :", s)
    print("nombres de victoires :", v)
    if s != 0:
        print("Pourcentage de victoires :", v/s)
        print(" ")
        print(table)

def rencontrastat(nomequipe1, nomequipe2):
    table1 = df[(df['Team1'] == nomequipe1) & (df['Team2'] == nomequipe2)]
    table2 = df[(df['Team1'] == nomequipe2) & (df['Team2'] == nomequipe1)]
    table = df[((df['Team1'] == nomequipe1) & (df['Team2'] == nomequipe2)) | ((df['Team1'] == nomequipe2) & (df['Team2'] == nomequipe1))]
    if table.empty:
        print("aucune rencontre effectuée entre ces deux équipes")
    else:
        s, v1, v2 = len(table), 0, 0

        for i in table1.index:
            if table1.loc[i]['ScoreTeam1'] > table1.loc[i]['ScoreTeam2']:
                v1 += 1
            else:
                v2 += 1

        for i in table2.index:
            if table2.loc[i]['ScoreTeam2'] > table2.loc[i]['ScoreTeam1']:
                v1 += 1
            else:
                v2 += 1

        print(nomequipe1, "VS", nomequipe2)
        print("nombre de victoires pour", nomequipe1, ":", v1)
        print("nombre de victoires pour", nomequipe2, ":", v2)
        print(table)

#obtainallequipes()
statequipe('KC Pioneers')
#rencontrastat('G2 Esport', 'Pionneers')