import pandas as pd

df0 = pd.DataFrame(columns=['idrencontre', 'Nom de la competition', 'Type du match', 'Date', 'Team1', 'Team2', 'ScoreTeam1', 'ScoreTeam2', 'player1', 'player2', 'player3', 'player4', 'player5', 'player6',
                           'SCPG1', 'GPG1', 'APG1', 'SAPG1', 'SHPG1', 'SCPG2', 'GPG2', 'APG2', 'SAPG2', 'SHPG2', 'SCPG3', 'GPG3', 'APG3', 'SAPG3', 'SHPG3', 'SCPGT1', 'GPGT1', 'APGT1', 'SAPGT1', 'SHPGT1', 'SCPG4', 'GPG4', 'APG4', 'SAPG4', 'SHPG4', 'SCPG5', 'GPG5', 'APG5', 'SAPG5', 'SHPG5', 'SCPG6', 'GPG6', 'APG6', 'SAPG6', 'SHPG6', 'SCPGT2', 'GPGT2', 'APGT2', 'SAPGT2', 'SHPGT2',
                           'rating1', 'rating2', 'rating3', 'rating4', 'rating5', 'rating6'])

df = pd.read_csv(r"myfile3.csv", sep=";")

def allplayers():
    L = []
    for i in df['player1']:
        if i not in L:
            L.append(i)
    for i in df['player2']:
        if i not in L:
            L.append(i)
    for i in df['player3']:
        if i not in L:
            L.append(i)
    for i in df['player4']:
        if i not in L:
            L.append(i)
    for i in df['player5']:
        if i not in L:
            L.append(i)
    for i in df['player6']:
        if i not in L:
            L.append(i)
    return L

def allequipes():
    L = []
    for i in df['Team1']:
        if i not in L:
            L.append(i)
    for i in df['Team2']:
        if i not in L:
            L.append(i)
    return L


def donneesequipe(nomequipe):
    newdf = df[(df['Team1'] == nomequipe) | (df['Team2'] == nomequipe)]
    return newdf.reset_index(drop = True)

def donneesrencontre(nomequipe1, nomequipe2):
    newdf = df[((df['Team1'] == nomequipe1) & (df['Team2'] == nomequipe2)) | ((df['Team1'] == nomequipe2) & (df['Team2'] == nomequipe1))]
    return newdf.reset_index(drop=True)

def donneesplayer(nomplayer):
    newdf = df[(df['player1'] == nomplayer) | (df['player2'] == nomplayer) | (df['player3'] == nomplayer) | (df['player4'] == nomplayer) | (df['player5'] == nomplayer) | (df['player6'] == nomplayer)]
    return newdf.reset_index(drop = True)


def traitementtableplayer(nomplayer):
    df = donneesplayer(nomplayer)
    newdf = pd.DataFrame(columns=['idrencontre', 'Nom de la competition', 'Type du match', 'Date', 'Team', 'player', 'SCPG', 'GPG', 'APG', 'SAPG', 'SHPG', 'rating'])
    for i in df.index:
        if df.loc[i]['player1'] == nomplayer:
            L = df.loc[[i], ['idrencontre', 'Nom de la competition', 'Type du match', 'Date', 'Team1', 'player1', 'SCPG1', 'GPG1', 'APG1', 'SAPG1', 'SHPG1', 'rating1']]

        if df.loc[i]['player2'] == nomplayer:
            L = df.loc[[i], ['idrencontre', 'Nom de la competition', 'Type du match', 'Date', 'Team1', 'player2', 'SCPG2', 'GPG2', 'APG2', 'SAPG2', 'SHPG2', 'rating2']]

        if df.loc[i]['player3'] == nomplayer:
            L = df.loc[[i], ['idrencontre', 'Nom de la competition', 'Type du match', 'Date', 'Team1', 'player3', 'SCPG3', 'GPG3', 'APG3', 'SAPG3', 'SHPG3', 'rating3']]

        if df.loc[i]['player4'] == nomplayer:
            L = df.loc[[i], ['idrencontre', 'Nom de la competition', 'Type du match', 'Date', 'Team2', 'player4', 'SCPG4', 'GPG4', 'APG4', 'SAPG4', 'SHPG4', 'rating4']]

        if df.loc[i]['player5'] == nomplayer:
            L = df.loc[[i], ['idrencontre', 'Nom de la competition', 'Type du match', 'Date', 'Team2', 'player5', 'SCPG5', 'GPG5', 'APG5', 'SAPG5', 'SHPG5', 'rating5']]

        if df.loc[i]['player6'] == nomplayer:
            L = df.loc[[i], ['idrencontre', 'Nom de la competition', 'Type du match', 'Date', 'Team2', 'player6', 'SCPG6', 'GPG6', 'APG6', 'SAPG6', 'SHPG6', 'rating6']]
        L.columns = ['idrencontre', 'Nom de la competition', 'Type du match', 'Date', 'Team', 'player', 'SCPG', 'GPG', 'APG', 'SAPG', 'SHPG', 'rating']
        newdf = pd.concat([newdf,L])
    return newdf

def traitementtableequipeonly(nomequipe):
    df = donneesequipe(nomequipe)
    newdf = pd.DataFrame(columns=['idrencontre', 'Nom de la competition', 'Type du match', 'Date', 'Team', 'player1', 'player2', 'player3', 'SCPGT', 'GPGT', 'APGT', 'SAPGT', 'SHPGT'])
    for i in df.index:
        if df.loc[i]['Team1'] == nomequipe:
            L = df.loc[[i], ['idrencontre', 'Nom de la competition', 'Type du match', 'Date', 'Team1','player1', 'player2', 'player3', 'SCPGT1', 'GPGT1', 'APGT1', 'SAPGT1', 'SHPGT1']]
        if df.loc[i]['Team2'] == nomequipe:
            L = df.loc[[i], ['idrencontre', 'Nom de la competition', 'Type du match', 'Date', 'Team2','player4', 'player5', 'player6', 'SCPGT2', 'GPGT2', 'APGT2', 'SAPGT2', 'SHPGT2']]
        L.columns = ['idrencontre', 'Nom de la competition', 'Type du match', 'Date', 'Team', 'player1', 'player2', 'player3', 'SCPGT', 'GPGT', 'APGT', 'SAPGT', 'SHPGT']
        newdf = pd.concat([newdf, L])
    return newdf


def statrencontre(nomequipe1, nomequipe2):
    df = donneesrencontre(nomequipe1, nomequipe2)
    print(statequipe(nomequipe1), statequipe(nomequipe2))
    v1, v2 = 0, 0
    s = len(df)
    if s == 0:
        print("aucunes rencontres effectuées entre ces deux équipes")
    else:
        for i in df.index:
            if df.loc[i]['Team1'] == nomequipe1:
                if df.loc[i]['ScoreTeam1'] > df.loc[i]['ScoreTeam2']:
                    v1 += 1
                else:
                    v2 +=1
            else:
                if df.loc[i]['ScoreTeam1'] > df.loc[i]['ScoreTeam2']:
                    v2 += 1
                else:
                    v1 +=1
        print(nomequipe1, ":", v1, "victoires sur", s)
        print(nomequipe2, ":", v2, "victoires sur", s)
        print(df)

def statplayer(nomplayer):
    df = traitementtableplayer(nomplayer)
    dftosum = df.loc[:,['SCPG', 'GPG', 'APG', 'SAPG', 'SHPG', 'rating']].mean()
    print(nomplayer)
    print(dftosum)
    return dftosum

def statequipe(nomequipe):
    df = traitementtableequipeonly(nomequipe)
    dftosum = df.loc[:, ['SCPGT', 'GPGT', 'APGT', 'SAPGT', 'SHPGT']].mean()
    players = playersteam(nomequipe)
    print(nomequipe,':', players)
    print(dftosum)
    for i in players:
        statplayer(i)
    return dftosum


def playersteam(nomequipe):
    df = traitementtableequipeonly(nomequipe)
    players = []
    for i in df['player1']:
        if i not in players:
            players.append(i)
    for i in df['player2']:
        if i not in players:
            players.append(i)
    for i in df['player3']:
        if i not in players:
            players.append(i)
    return players


def statsallplayer():
    players = allplayers()
    newdf = pd.DataFrame(columns=['SCPG', 'GPG', 'APG', 'SAPG', 'SHPG', 'rating'])
    s, name = 0, ""
    for i in players :
        df = statplayer(i)
        if df['SCPG'] > s:
            s, name = df['SCPG'], i
        L = [df['SCPG']] + [df['GPG']] + [df['APG']] + [df['SAPG']] + [df['SHPG']] + [df['rating']]
        newdf.loc[i] = L
    print('le meilleur joueur est :', name, 'avec un score de',round(s, 1))
    print(newdf.mean())
    return newdf

def statsallteam():
    equipes = allequipes()
    newdf = pd.DataFrame(columns=['SCPGT', 'GPGT', 'APGT', 'SAPGT', 'SHPGT'])
    s, name = 0, ""
    for i in equipes:
        df = statequipe(i)
        if df['SCPGT'] > s:
            s, name = df['SCPGT'], i
        L = [df['SCPGT']] + [df['GPGT']] + [df['APGT']] + [df['SAPGT']] + [df['SHPGT']]
        newdf.loc[i] = L
    print('la meilleure equipe est :', name, 'avec un score de', round(s, 1))
    print(newdf.mean())
    return newdf



equipes = allequipes()
print("nombre d'équipes :", len(equipes), equipes)
players = allplayers()
#print("nombre de joueurs RLCS", len(players), players)

#statsallplayer()
#statsallteam()

#statequipe('Team Vitality')
#statequipe('Solary')
#statequipe('Dignitas')

#statplayer('M0nkey M00n')
#statplayer('Kaydop')

statrencontre('Team Vitality', 'Solary')

