match = {
    equipe1 : {
        id_equipe: int,
        name_equipe: str,
        joueurs : [
            {joueur1: int,
             name: str,
             actions:{
                points: float,
                goals: int,
                assists: int,
                saves: int,
                shots: int
                 }
            },
            {joueur2: int,
             name: str,
             actions:{
                points: float,
                goals: int,
                assists: int,
                saves: int,
                shots: int
                 }
            },
            {joueur3: int,
             name: str,
             actions:{
                points: float,
                goals: int,
                assists: int,
                saves: int,
                shots: int
                 }
            },
        ]
    }
}