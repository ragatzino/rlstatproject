from sqlalchemy.orm import Session
from . import models, schemas

def get_equipe(db: Session, equipe_id: int):
    return db.query(models.Equipe).filter(models.Equipe.id == equipe_id).first()

def get_equipe_by_name(db: Session, name: str):
    return db.query(models.Equipe).filter(models.Equipe.name == name).first()

def get_equipes(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Equipe).offset(skip).limit(limit).all()

def create_equipe(db: Session, equipe: schemas.EquipeCreate):
    db_equipe = models.Equipe(name=equipe.name)
    db.add(db_equipe)
    db.commit()
    db.refresh(db_equipe)
    return db_equipe

def get_matchs(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Match).offset(skip).limit(limit).all()

def create_equipe_match(db: Session, match:schemas.MatchCreate, equipe_id: int):
    db_match = models.Match(**match.dict(), id_equipe=equipe_id)
    db.add(db_match)
    db.commit()
    db.refresh(db_match)
    return db_match
