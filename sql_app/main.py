from typing import List
from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session
from . import crud, models, schemas
from .database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@app.get("/equipes/", response_model=List[schemas.Equipe])
def read_equipes(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    equipes = crud.get_equipes(db, skip=skip, limit=limit)
    return equipes

@app.get("/equipes/{equipe_id}", response_model=schemas.Equipe)
def read_equipe(equipe_id: int, db: Session = Depends(get_db)):
    db_equipe = crud.get_equipe(db, equipe_id=equipe_id)
    if db_equipe is None:
        raise HTTPException(status_code=404, detail="Equipe not found")
    return db_equipe

@app.get("/matchs/", response_model=List[schemas.Match])
def read_matchs(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    matchs = crud.get_matchs(db, skip=skip, limit=limit)
    return matchs

@app.post("/equipes/{equipe_id}/matchs/", response_model=schemas.Match)
def create_match_for_equipe(equipe_id: int, match: schemas.MatchCreate, db: Session = Depends(get_db)):
    return crud.create_equipe_match(db=db, match=match, equipe_id=equipe_id)

@app.post("/equipes/", response_model=schemas.Equipe)
def create_equipe(equipe: schemas.EquipeCreate, db: Session = Depends(get_db)):
    db_equipe = crud.get_equipe_by_name(db, name=equipe.name)
    if db_equipe:
        raise HTTPException(status_code=400, detail="Team already registered")
    return crud.create_equipe(db=db, equipe=equipe)
