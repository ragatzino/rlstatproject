from typing import List, Optional
from pydantic import BaseModel

class MatchBase(BaseModel):
    score: int

class MatchCreate(MatchBase):
    pass

class Match(MatchBase):
    id: int
    id_equipe: int
    class Config:
        orm_mode = True

class EquipeBase(BaseModel):
    name: str

class EquipeCreate(EquipeBase):
    pass

class Equipe(EquipeBase):
    id: int
    matchs: List[Match] = []
    class Config:
        orm_mode = True
